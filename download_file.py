import os, base64, urllib2

#Config settings
url = "http://bda.retroroms.net/"
directory = "downloads/mame/currentroms/"
rom = "3ds.zip"

username = "timmo710"
password = "password123"

#setup our login header
request = urllib2.Request(url + directory + rom)
base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
request.add_header("Authorization", "Basic %s" % base64string) 

def dlfile():
    try:
        result = urllib2.urlopen(request)
        print "downloading " + rom

        # Open our local file for writing
        with open(os.path.basename(rom), "wb") as local_file:
            local_file.write(result.read())

    #handle errors
    except urllib2.HTTPError, e:
        print "HTTP Error:", e.code, url
    except urllib2.URLError, e:
        print "URL Error:", e.reason, url



def main():
	dlfile()

if __name__ == '__main__':
    main()
